package bookshelf;

import bookshelf.book.BookDataStructure;
import com.google.gson.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.lang.invoke.MethodHandles;


public class JsonManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    public static BookDataStructure readJsonBooks(String path) {
        BookDataStructure booksData = new BookDataStructure();
        try {
            Gson gson = new Gson();
            booksData = gson.fromJson(new BufferedReader(new FileReader(path)), BookDataStructure.class);
            if (booksData == null) throw new IOException("Empty input file");
        } catch (IOException e) {
            LOGGER.error("Caught IOException while trying to read file: " + path + " " + e);
            System.exit(1);
        } catch (Exception e) {
            LOGGER.error("Caught Exception " + e);
            e.printStackTrace();
        }
        return booksData;
    }

    public static String toPrettyJson(Object object) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return "<pre>" + gson.toJson(object) + "</pre>";
    }
}
