package bookshelf.book;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BookBuilder {
    private BookDataStructure.Item item;

    public BookBuilder setItem(BookDataStructure.Item item) {
        this.item = item;
        return this;
    }

    public Book createBook() {
        Book book = new Book();

        book.setTitle(item.volumeInfo.title);
        book.setSubtitle(item.volumeInfo.subtitle);
        book.setPublisher(item.volumeInfo.publisher);
        book.setPublishedDate(parseDateMillis(item.volumeInfo.publishedDate));
        book.setDescription(item.volumeInfo.description);
        book.setPageCount(item.volumeInfo.pageCount);
        book.setThumbnailUrl(item.volumeInfo.imageLinks.thumbnail);
        book.setLanguage(item.volumeInfo.language);
        book.setPreviewLink(item.volumeInfo.previewLink);
        book.setAverageRating(item.volumeInfo.averageRating);

        List<String> authors = new ArrayList<String>();
        if (item.volumeInfo.authors != null) {
            authors = new ArrayList<String>(Arrays.asList(item.volumeInfo.authors));
        }
        book.setAuthors(authors);

        List<String> categories = new ArrayList<>();
        if (item.volumeInfo.categories != null) {
            categories = new ArrayList<String>(Arrays.asList(item.volumeInfo.categories));
        }
        book.setCategories(categories);

        for (BookDataStructure.IndustryIdentifiers industryId : item.volumeInfo.industryIdentifiers) {
            if (industryId.type.equals("ISBN_13")) {
                book.setIsbn(industryId.identifier);
            } else {
                book.setIsbn(item.id);
            }
        }
        return book;
    }

    public Long parseDateMillis(String date) {
        String[] acceptableDateFormats = new String[]{"yyyy-MM-dd", "yyyy"};
        if (date == null) {
            return null;
        }
        SimpleDateFormat simpleDateFormat;
        for (String dateFormat : acceptableDateFormats) {
            try {
                simpleDateFormat = new SimpleDateFormat(dateFormat);
                return simpleDateFormat.parse(date).getTime();
            } catch (ParseException e) {
                continue;
            }
        }
        return null;
    }
}