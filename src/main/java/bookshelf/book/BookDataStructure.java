package bookshelf.book;

import java.util.Arrays;

public class BookDataStructure {
    public Item[] items;

    public static class ImageLinks {
        public String smallThumbnail;
        public String thumbnail;
    }

    public static class IndustryIdentifiers {
        public String type;
        public String identifier;
    }

    public static class Item {
        public String id;
        public VolumeInfo volumeInfo;
    }

    public static class VolumeInfo {
        public String title;
        public String subtitle;
        public String publisher;
        public IndustryIdentifiers[] industryIdentifiers;
        public String[] authors;
        public String publishedDate;
        public String description;
        public int pageCount;
        public String[] categories;
        public double averageRating;
        public String language;
        public ImageLinks imageLinks;
        public String previewLink;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BookDataStructure)) return false;

        BookDataStructure that = (BookDataStructure) o;
        return Arrays.equals(items, that.items);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(items);
    }
}
