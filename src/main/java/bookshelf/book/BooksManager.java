package bookshelf.book;

import java.util.*;
import java.util.stream.Collectors;

public class BooksManager {

    public void setBooksList(List<Book> booksList) {
        this.booksList = booksList;
    }

    protected List<Book> booksList;

    public BooksManager(){}

    public BooksManager(BookDataStructure booksData) {
        this.booksList = createBooksList(booksData);
    }

    protected List<Book> createBooksList(BookDataStructure booksData) {
        List<Book> booksList = new ArrayList<>();
        for (BookDataStructure.Item item : booksData.items) {
            booksList.add(new BookBuilder().setItem(item).createBook());
        }
        return booksList;
    }

    protected Map<String, List<Double>> getRatingMap() {
        Map<String, List<Double>> authorsMap = new HashMap<String, List<Double>>();
        for (Book book: booksList) {
            if(book.getAverageRating() == null) continue;
            if(book.getAuthors().size() == 0) continue;

            for(String author: book.getAuthors()) {
                if (authorsMap.containsKey(author)) {
                    authorsMap.get(author).add(book.getAverageRating());
                } else {
                    authorsMap.put(author, new ArrayList<>(Arrays.asList(book.getAverageRating())));
                }
            }
        }
        return authorsMap;
    }

    public List<Author> getAuthorsRating() {
        List<Author> authorsRating = new ArrayList<Author>();
        Map<String, List<Double>> ratingMap = getRatingMap();

        for(String authorName: ratingMap.keySet()) {
            List<Double> ratingList = ratingMap.get(authorName);
            double rating = ratingList.stream().mapToDouble(f -> f).sum() / ratingList.size();
            authorsRating.add(new Author(authorName, rating));
        }
        authorsRating.sort(Comparator.comparing(Author::getRating).reversed());
        return authorsRating;
    }

    public Book getBookByIsbn(String isbn) {
        try{
            return booksList.stream().filter(b -> b.getIsbn().equals(isbn)).findFirst().get();
        } catch (NoSuchElementException e){
            return null;
        }
    }

    public List<Book> getBooksByCategory(String category) {
        return booksList.stream().filter(b -> b.getCategories()
                .contains(category)).collect(Collectors.toList());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BooksManager that = (BooksManager) o;

        return booksList != null ? booksList.equals(that.booksList) : that.booksList == null;
    }

    @Override
    public int hashCode() {
        return booksList != null ? booksList.hashCode() : 0;
    }
}
