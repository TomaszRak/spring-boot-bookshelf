package bookshelf.book;

public class Author {
    public String getName() {
        return name;
    }

    public Double getRating() {
        return rating;
    }

    private String name;
    private Double rating;

    public Author(String authorName, double rating) {
        this.name = authorName;
        this.rating = rating;
    }
}
