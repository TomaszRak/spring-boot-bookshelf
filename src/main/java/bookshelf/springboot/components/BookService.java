package bookshelf.springboot.components;

import bookshelf.JsonManager;
import bookshelf.book.BookDataStructure;
import bookshelf.book.BooksManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class BookService {

    private BookDataStructure booksData;
    protected BooksManager booksManager;
    private String dataSource;

    public void initFields() {
        this.booksData = JsonManager.readJsonBooks(dataSource);
        this.booksManager = new BooksManager(booksData);
    }
    public void setDataSource(String dataSource) {
        this.dataSource = dataSource;
    }
}
