package bookshelf.springboot.components;

import bookshelf.JsonManager;
import bookshelf.book.Book;
import bookshelf.exceptions.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.lang.invoke.MethodHandles;

@RestController
public class BookRequestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Autowired
    protected BookService bookService;

    @RequestMapping("/api/book/{bookId}")
    @ResponseBody
    String getBookByIsbn(@PathVariable("bookId") String bookId) throws NotFoundException {
        Book book = bookService.booksManager.getBookByIsbn(bookId);
        if (book == null) {
            LOGGER.warn("Book not found, isbn: " + bookId);
            throw new NotFoundException();
        }
        return JsonManager.toPrettyJson(book);
    }

    @RequestMapping("/api/category/{bookCategory}/books")
    @ResponseBody
    public String getBooksByCategory(@PathVariable("bookCategory") String bookCategory) {
        return JsonManager.toPrettyJson(bookService.booksManager.getBooksByCategory(bookCategory));
    }

    @RequestMapping("/api/rating")
    @ResponseBody
    public String getAuthorsRating() {
        return JsonManager.toPrettyJson(bookService.booksManager.getAuthorsRating());
    }
}
