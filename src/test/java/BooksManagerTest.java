import bookshelf.book.Book;
import bookshelf.book.BookDataStructure;
import bookshelf.book.BooksManager;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class BooksManagerTest {

    private List<Book> bookList;

    @Test
    void getBookByIsbnTest() {
        bookList = new ArrayList<Book>();

        Book book = new Book();
        book.setIsbn("testIsbn");

        Book book2 = new Book();
        book2.setIsbn("otherTestIsbn");

        bookList.add(book);
        bookList.add(book2);

        BooksManager booksManager = new BooksManager();
        booksManager.setBooksList(bookList);
        assertEquals(booksManager.getBookByIsbn("testIsbn"), book);
        assertNotEquals(booksManager.getBookByIsbn("otherTestIsbn"), book);
    }

    @Test
    void getBooksByCategoryTest() {

        Book book1 = new Book();
        book1.setCategories(new ArrayList<String>() {{
            add("testCategory");
            add("testCategory2");
        }});

        Book book2 = new Book();
        book2.setCategories(new ArrayList<String>() {{
            add("testCategory");
        }});

        bookList = new ArrayList<Book>();
        bookList.add(book1);
        bookList.add(book2);

        BooksManager booksManager = new BooksManager();
        booksManager.setBooksList(bookList);

        List<Book> resultList = booksManager.getBooksByCategory("testCategory");
        assertTrue(resultList.contains(book1) && resultList.contains(book2));

        resultList = booksManager.getBooksByCategory("testCategory2");
        assertTrue(resultList.contains(book1));
        assertFalse(resultList.contains(book2));
    }
}
