import bookshelf.book.BookDataStructure;
import bookshelf.JsonManager;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class JsonManagerTest {

    @Test
    void readJsonBooksTest() {
        BookDataStructure booksData = JsonManager.readJsonBooks("src/test/test_resources/samplebook.json");
        assertEquals(booksData.getClass(), BookDataStructure.class);
        assertEquals(booksData.items[0].id, "87totx4p3ZcC");
        assertEquals(booksData.items[0].volumeInfo.title, "Java in a Time of Revolution");
        assertEquals(booksData.items[0].volumeInfo.subtitle, "Occupation and Resistance, 1944-1946");
        assertEquals(booksData.items[0].volumeInfo.authors[0], "Benedict Anderson");
        assertEquals(booksData.items[0].volumeInfo.publisher, "Equinox Publishing");
        assertEquals(booksData.items[0].volumeInfo.publishedDate, "2005-12-01");
        assertEquals(booksData.items[0].volumeInfo.description, "someTestDescription");
        assertEquals(booksData.items[0].volumeInfo.industryIdentifiers[0].type, "ISBN_13");
        assertEquals(booksData.items[0].volumeInfo.industryIdentifiers[0].identifier, "9789793780146");
        assertEquals(booksData.items[0].volumeInfo.industryIdentifiers[1].type, "ISBN_10");
        assertEquals(booksData.items[0].volumeInfo.industryIdentifiers[1].identifier, "9793780142");
        assertEquals(booksData.items[0].volumeInfo.pageCount, 494);
        assertEquals(booksData.items[0].volumeInfo.categories[0], "History");
        assertEquals(booksData.items[0].volumeInfo.imageLinks.smallThumbnail, "testSmallThumbnailLink");
        assertEquals(booksData.items[0].volumeInfo.imageLinks.thumbnail, null);
        assertEquals(booksData.items[0].volumeInfo.language, "en");
        assertEquals(booksData.items[0].volumeInfo.previewLink, "testPreviewLink");
        assertEquals(booksData.items[0].volumeInfo.averageRating, 4.5);
    }

}
