import bookshelf.book.Book;
import bookshelf.book.BookBuilder;
import bookshelf.book.BookDataStructure;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BookBuilderTest {

    BookDataStructure.Item item = new BookDataStructure.Item();

    @Test
    void createBookTest() throws Exception {
        BookDataStructure.Item item = new BookDataStructure.Item();
        item.volumeInfo = new BookDataStructure.VolumeInfo();
        item.volumeInfo.imageLinks = new BookDataStructure.ImageLinks();
        item.volumeInfo.industryIdentifiers = new BookDataStructure.IndustryIdentifiers[]{};

        assertEquals(new BookBuilder().setItem(item).createBook().getClass(), Book.class);
    }

    @Test
    void parseDateYearMonthDayTest() {
        assertEquals(Long.valueOf(1314568800000L),new BookBuilder().parseDateMillis("2011-08-29"));
    }
    @Test
    void parseDateYearTest() {
        assertEquals(Long.valueOf(1104534000000L),new BookBuilder().parseDateMillis("2005"));
    }
}
